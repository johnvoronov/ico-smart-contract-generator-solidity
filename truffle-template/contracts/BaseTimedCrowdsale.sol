pragma solidity ^0.4.21;

import './BaseWithdrawCrowdsale.sol';
import "openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol";

contract BaseTimedCrowdsale is BaseWithdrawCrowdsale, TimedCrowdsale {
    function BaseTimedCrowdsale(uint256 _openingTime, uint256 _closingTime) public
    TimedCrowdsale(_openingTime, _closingTime)
    {
    }

    // Overrided this function to add validation for hasClosed
    function withdrawAvailable() public view returns (bool) {
        return hasClosed() && super.withdrawAvailable();
    }
}
