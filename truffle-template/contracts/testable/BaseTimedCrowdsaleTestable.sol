pragma solidity ^0.4.21;

import './../BaseWithdrawCrowdsale.sol';
import 'openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol';

contract BaseTimedCrowdsaleTestable is BaseWithdrawCrowdsale, TimedCrowdsale {
    function BaseTimedCrowdsaleTestable(uint256 _openingTime, uint256 _closingTime, uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token)
    TimedCrowdsale(_openingTime, _closingTime)
    {
    }

    // Withdraw is available only if the Crowdsale is over
    function _validateWithdraw(address _from, address _to, uint256 _tokenAmount) internal {
        require(hasClosed());

        super._validateWithdraw(_from, _to, _tokenAmount);
    }
}
