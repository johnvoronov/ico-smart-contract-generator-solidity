pragma solidity ^0.4.21;

import './../BaseWithdrawCrowdsale.sol';
import './../BaseBurnableToken.sol';


contract BaseBurnableCrowdsaleTestable is BaseWithdrawCrowdsale {
    function BaseBurnableCrowdsaleTestable(uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token) {
    }

    // Override this function to send the tokens
    function _withdraw(address _to, uint256 _tokenAmount) internal {
        require(BaseBurnableToken(token).transfer(_to, _tokenAmount));
    }
}
