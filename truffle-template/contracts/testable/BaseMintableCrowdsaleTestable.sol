pragma solidity ^0.4.21;

import './../BaseWithdrawCrowdsale.sol';
import './../BaseMintableToken.sol';

contract BaseMintableCrowdsaleTestable is BaseWithdrawCrowdsale {
    function BaseMintableCrowdsaleTestable(uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token) {
    }

    // Override this function to send the tokens
    function _withdraw(address _to, uint256 _tokenAmount) internal {
        require(BaseMintableToken(token).mint(_to, _tokenAmount));
    }
}
