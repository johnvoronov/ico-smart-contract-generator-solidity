pragma solidity ^0.4.21;

import './../BaseWithdrawCrowdsale.sol';
import './../BaseTimeDiscountCrowdsale.sol';

contract BaseTimeDiscountCrowdsaleTestable is BaseWithdrawCrowdsale, BaseTimeDiscountCrowdsale {
    function BaseTimeDiscountCrowdsaleTestable(uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token)
    BaseTimeDiscountCrowdsale()
    {
    }

    // Override this function to set the discounts
    function setDiscounts() internal {
        discountAvailable = [50, 40, 30];
        discountByTime[50] = now + 60 * 60 * 24;
        discountByTime[40] = now + 60 * 60 * 24 * 2;
        discountByTime[30] = now + 60 * 60 * 24 * 3;
    }
}
