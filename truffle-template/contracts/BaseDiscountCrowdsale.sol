pragma solidity ^0.4.21;

import './BaseWithdrawCrowdsale.sol';
import 'openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

contract BaseDiscountCrowdsale is BaseWithdrawCrowdsale {
  using SafeMath for uint256;

  // Discounts mapping
  mapping(uint8 => uint256) public discountByTokens;

  // Available discount percents
  uint8[] public discountAvailable;

  function BaseDiscountCrowdsale() public
  {
    setDiscounts();
  }

  // Override this function to set required discounts
  function setDiscounts() internal {
  }

  // Returns available discount
  function getDiscount() public view returns (uint8)
  {
    for (uint i = 0; i < discountAvailable.length; i++) {
      uint8 discount = discountAvailable[i];

      if (totalSupply_ < discountByTokens[discount]) {
        return discount;
      }
    }

    return 0;
  }

  // Overrided this function to make it use discount system
  function _getTokenAmount(uint256 _weiAmount) internal view returns (uint256) {
    uint256 withoutDiscount = _weiAmount.mul(rate);
    uint8 currentDiscount = getDiscount();

    if (currentDiscount == 0) {
      return withoutDiscount;
    }

    return withoutDiscount.add(withoutDiscount.mul(currentDiscount).div(100));
  }
}
