pragma solidity ^0.4.21;

import './BaseWithdrawCrowdsale.sol';
import './BaseMintableToken.sol';


contract BaseMintableCrowdsale is BaseWithdrawCrowdsale {
    // Override this function to send the tokens
    function _withdraw(address _to, uint256 _tokenAmount) internal {
        require(BaseMintableToken(token).mint(_to, _tokenAmount));
    }
}
