const Crowdsale = artifacts.require('./BaseWithdrawCrowdsale.sol');
const Token = artifacts.require("./BaseBurnableToken.sol");

// 2 tokens for every wei or 0.5 tokens for every eth
const rate = 2;

contract('BaseWithdrawCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(rate, owner, token.address, {
            from: owner
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks that only owner can call the addTokens method', async () => {
        try {
            await crowdsale.addTokens(anotherAccount, 1000, {
                from: anotherAccount
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the addTokens method works properly', async () => {
        const weiAmount = 1000;

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        const weiRaised = await crowdsale.weiRaised();
        const tokensRaised = await crowdsale.totalSupply();
        const balanceOf = await crowdsale.balanceOf(anotherAccount);

        assert.equal(weiAmount, weiRaised.toNumber());
        assert.equal(weiAmount * rate, tokensRaised.toNumber());
        assert.equal(weiAmount * rate, balanceOf.toNumber());
    });

    it('checks that only owner can call the withdrawByOwner method', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        try {
            await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
                from: anotherAccount
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdrawByOwner method adds nothing when the balance is 0', async () => {
        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });

        const balance = await token.balanceOf(anotherAccount);

        assert.equal(balance.toNumber(), 0);
    });

    it('checks that the withdraw method dies with an exception when the balance is 0', async () => {
        try {
            await crowdsale.withdraw({
                from: anotherAccount
            });
        } catch (err) {
            assert.ok(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdrawByOwner method works fine', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });

        const contractBalance = await crowdsale.balanceOf(anotherAccount);

        // To make sure that the balance was nulled
        assert.equal(0, contractBalance.toNumber());
    });

    it('checks that the withdraw method works fine', async () => {
        // 10 tokens
        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('5', 'ether')
        });

        await crowdsale.withdraw({
            from: anotherAccount
        });

        const contractBalance = await crowdsale.balanceOf(anotherAccount);

        // To make sure that the balance was nulled
        assert.equal(0, contractBalance.toNumber());
    });

    it('checks that only owner can call the withdrawMultiple method', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        try {
            await crowdsale.withdrawMultiple([anotherAccount], [anotherAccount], {
                from: anotherAccount
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdrawMultiple method does not fail when one of the balances is 0', async () => {
        const weiAmount = 1000;
        const thirdAccount = accounts[2];

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        await crowdsale.withdrawMultiple([anotherAccount, thirdAccount], [anotherAccount, thirdAccount], {
            from: owner
        });

        const anotherBalance = await crowdsale.balanceOf(anotherAccount);
        const thirdBalance = await crowdsale.balanceOf(thirdAccount);

        // To make sure that the balance was nulled
        assert.equal(0, anotherBalance.toNumber());
        assert.equal(0, thirdBalance.toNumber());
    });

    it('checks that the withdrawMultiple method works fine', async () => {
        const weiAmount = 1000;
        const thirdAccount = accounts[2];

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });
        await crowdsale.addTokens(thirdAccount, weiAmount, {
            from: owner
        });

        await crowdsale.withdrawMultiple([anotherAccount, thirdAccount], [anotherAccount, thirdAccount], {
            from: owner
        });

        const secondContractBalance = await crowdsale.balanceOf(anotherAccount);
        const thirdContractBalance = await crowdsale.balanceOf(thirdAccount);

        // To make sure that the balance was nulled
        assert.equal(0, secondContractBalance.toNumber());
        assert.equal(0, thirdContractBalance.toNumber());
    });

});