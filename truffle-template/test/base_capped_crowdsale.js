const Crowdsale = artifacts.require('./BaseCappedCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseMintableToken.sol");

const name = 'TEST';
const symbol = 'TST';
const decimals = 18;

// 2 tokens for every wei or 2 tokens for every eth
const rate = 2;
const softLimit = 1000;
const hardLimit = 2000;

contract('BaseCappedCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(
            softLimit,
            hardLimit,
            rate,
            owner,
            token.address,
            {
                from: owner
            }
        );

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks that there is not way to add a new tokens when the hard limit is reached', async () => {
        await crowdsale.addTokens(anotherAccount, hardLimit, {
            from: owner
        });

        try {
            await crowdsale.addTokens(anotherAccount, 1, {
                from: owner
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdraw method would not be available until the soft limit is reached', async () => {
        const weiAmount = 999;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        try {
            await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
                from: owner
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdrawByOwner method would be available after the soft limit is reached', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });
    });

    it('checks that the softLimitReached method works fine', async () => {
        const weiAmount = 500;

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        let softLimitReached = await crowdsale.softLimitReached();

        assert.equal(false, softLimitReached);

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        softLimitReached = await crowdsale.softLimitReached();

        assert.equal(true, softLimitReached);
    });

    it('checks that the hardLimitReached method works fine', async () => {
        const weiAmount = 1000;

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        let hardLimitReached = await crowdsale.hardLimitReached();

        assert.equal(false, hardLimitReached);

        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        hardLimitReached = await crowdsale.hardLimitReached();

        assert.equal(true, hardLimitReached);
    });
});