const Crowdsale = artifacts.require('./BaseTimedCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseMintableToken.sol");

const seconds = require('time-funcs/seconds');

// 2 tokens for every wei or 0.5 tokens for every eth
const rate = 2;

const advanceBlock = () => {
    return new Promise((resolve, reject) => {
        web3.currentProvider.sendAsync({
            jsonrpc: '2.0',
            method: 'evm_mine',
            id: Date.now(),
        }, (err, res) => {
            return err ? reject(err) : resolve(res)
        })
    });
};


// To change the time of the latest block
const backToTheFuture = (seconds) => {
    // Increase time
    const id = Date.now();

    return new Promise((resolve, reject) => {
        web3.currentProvider.sendAsync({
            jsonrpc: '2.0',
            method: 'evm_increaseTime',
            params: [seconds],
            id: id,
        }, err1 => {
            if (err1) return reject(err1);

            web3.currentProvider.sendAsync({
                jsonrpc: '2.0',
                method: 'evm_mine',
                id: id + 1,
            }, (err2, res) => {
                return err2 ? reject(err2) : resolve(res)
            })
        })
    });
};

contract('BaseTimedCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;
    let blockTimestamp;

    // Mine a block
    before(async () => {
        await advanceBlock();
    });


    beforeEach(async () => {
        const lastBlock = await web3.eth.getBlock('latest');

        blockTimestamp = lastBlock.timestamp;

        const startTime = blockTimestamp + 5;
        const endTime = startTime + seconds({days: 2});

        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(
            startTime,
            endTime,
            rate,
            owner,
            token.address,
            {
                from: owner
            }
        );

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });

        // Move to the future for 60 seconds to have the crowdsale started
        await backToTheFuture(60);
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks that the withdrawByOwner method would not be available until the crowdsale is over', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        try {
            await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
                from: owner
            });
        } catch (err) {
            assert(err);

            return;
        }

        assert(false);
    });

    it('checks that the withdrawByOwner method would be available after the crowdsale is over', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        await backToTheFuture(seconds({days: 3}));

        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });

        const contractBalance = await crowdsale.balanceOf(anotherAccount);

        // To make sure that the balance was nulled
        assert.equal(0, contractBalance);
    });

    it('checks that the hasClosed method works properly', async () => {
        let hasClosed = await crowdsale.hasClosed();

        assert.equal(false, hasClosed);

        await backToTheFuture(seconds({days: 3}));

        hasClosed = await crowdsale.hasClosed();

        assert.equal(true, hasClosed);
    });
});