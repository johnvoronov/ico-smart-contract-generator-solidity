const Token = artifacts.require("./BaseMintableToken.sol");

const name = 'TEST';
const symbol = 'TST';
const decimals = 18;
const ownerSupply = 9999;

contract('BaseMintableToken', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];


    let contract;

    beforeEach(async () => {
        contract = await Token.new({
            from: owner
        });

        // Mint some tokens to owner
        await contract.mint(owner, ownerSupply);
    });

    it('makes sure that the contract was deployed', () => {
        assert.ok(contract.address);
    });

    it('checks params', async () => {
        let instanceName = await contract.name();
        let instanceSymbol = await contract.symbol();
        let instanceDecimals = await contract.decimals();

        assert.equal(name, instanceName);
        assert.equal(symbol, instanceSymbol);
        assert.equal(decimals, instanceDecimals);
    });

    it('sends tokens to another account', async () => {
        const amount = 1000;

        // First check if the first account would lose his tokens
        const ownerBeforeBalance = await contract.balanceOf(owner);

        assert.equal(
            ownerSupply,
            ownerBeforeBalance.toNumber()
        );

        await contract.transfer(anotherAccount, amount, {from: owner, gas: '1000000'});

        const ownerAfterBalance = await contract.balanceOf(owner);

        assert.equal(
            ownerSupply - amount,
            ownerAfterBalance.toNumber()
        );

        const anotherCurrentBalance = await contract.balanceOf(anotherAccount);

        assert.equal(
            amount,
            anotherCurrentBalance.toNumber()
        );
    });

    it('transfers ownership', async () => {
        const newOwner = accounts[1];

        const instanceOwner = await contract.owner();

        assert.equal(owner, instanceOwner);

        await contract.transferOwnership(newOwner, {from: owner});

        const afterOwner = await contract.owner();

        assert.equal(newOwner, afterOwner);
    });

    describe('Check mint cases', async () => {
        it('checks that only owner can mint tokens', async () => {
            try {
                await contract.mint(anotherAccount, 999, {
                    from: anotherAccount
                });
            } catch (err) {
                assert(err);

                return;
            }

            assert(false);
        });


        it('mints token to another account', async () => {
            const amount = 1000;

            const beforeBalance = await contract.balanceOf(anotherAccount);

            assert.equal(
                0,
                beforeBalance.toNumber()
            );

            await contract.mint(anotherAccount, amount, {
                from: owner
            });

            const afterBalance = await contract.balanceOf(anotherAccount);

            assert.equal(
                amount,
                afterBalance.toNumber()
            );
        });

        it('checks that only owner can finish minting', async () => {
            try {
                await contract.finishMinting({
                    from: anotherAccount
                });
            } catch (err) {
                assert(err);

                return;
            }

            assert(false);
        });

        it('checks that owner cannot mint after minting is finished', async () => {
            await contract.mint(anotherAccount, 999, {
                from: owner
            });

            await contract.finishMinting({
                from: owner
            });

            try {
                await contract.mint(anotherAccount, 999, {
                    from: owner
                });
            } catch (err) {
                assert(err);

                return;
            }

            assert(false);
        });
    });

    describe('Check allowance cases', () => {
        it('approves transfering', async () => {
            const amount = 1000;

            let anotherCurrentBalance, ownerCurrentBalance, anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentBalance = await contract.balanceOf(anotherAccount);
            ownerCurrentBalance = await contract.balanceOf(owner);
            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(0, anotherCurrentBalance.toNumber());
            assert.equal(ownerSupply, ownerCurrentBalance.toNumber());
            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.transferFrom(owner, anotherAccount, amount, {
                from: anotherAccount,
                gas: '1000000'
            });

            anotherCurrentBalance = await contract.balanceOf(anotherAccount);
            ownerCurrentBalance = await contract.balanceOf(owner);
            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentBalance.toNumber());
            assert.equal(ownerSupply - amount, ownerCurrentBalance.toNumber());
            assert.equal(0, anotherCurrentAllowance);
        });

        it('decreases approval', async () => {
            const amount = 1000;

            let anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.decreaseApproval(anotherAccount, amount / 2, {
                from: owner,
                gas: '1000000'
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount / 2, anotherCurrentAllowance.toNumber());
        });

        it('increases approval', async () => {
            const amount = 1000;

            let anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.increaseApproval(anotherAccount, amount, {
                from: owner,
                gas: '1000000'
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount * 2, anotherCurrentAllowance.toNumber());
        });
    });
});