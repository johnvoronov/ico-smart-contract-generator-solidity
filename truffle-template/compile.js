const path = require('path');
const fs = require('fs');
const solc = require('solc');

const contractsDir = path.resolve(__dirname, 'contracts');
const nodeModulesDir = path.resolve(__dirname, 'node_modules');

/**
 * Компиляция контрактов
 *
 * @returns {*|Function}
 */
function compile() {
    const MyCrowdsale = findContract('MyCrowdsale.sol');

    let inputs = {
        'MyCrowdsale.sol': MyCrowdsale
    };

    return solc.compile({
        sources: inputs
    }, 1, findImports);
}

/**
 * Поиск контракта
 *
 * @param file - файл контракта
 * @returns {*}
 */
function findContract(file) {
    let resolvedPath;

    if (file.indexOf('openzeppelin-solidity') !== -1) {
        resolvedPath = path.resolve(nodeModulesDir, file);
    } else {
        resolvedPath = path.resolve(contractsDir, file);
    }

    if (isFile(resolvedPath)) {
        return fs.readFileSync(resolvedPath).toString();
    } else {
        throw new Error(`File ${file} not found`);
    }
}

/**
 * Поиск импортируемого контракта
 *
 * @param path - путь к файлу контракта
 * @returns {*}
 */
function findImports(path) {
    try {
        return {
            'contents': findContract(path)
        };
    } catch (e) {
        return { error: e.message };
    }
}

/**
 *
 *
 * @param path - путь к файлу контракта
 * @returns {*}
 */
function isFile(path) {
    return fs.existsSync(path);
}

let compiledCode = compile();

fs.writeFile('compiled.json', JSON.stringify(compiledCode), (err) => {
    if (err) {
        throw new Error(err);
    }

    console.log('Compiled & saved');
});


