const request = require('supertest');
const app = require('../../app');

describe('generator', () => {
  let data;
  beforeEach(async () => {
    data = {
      token: {
        type: 'mintable',
        name: 'Bitcoin',
        symbol: 'BTC',
        decimals: 18
      },
      rate: 200,
      wallet: '0x1fe157F5592a7696c9708c145Dc75B3598577901',
      parents: {
        discountByTokens: {
          discount: 20,
          tokens: 2000
        },
        restrictByCap: {
          softCap: 10000,
          hardCap: 20000
        },
        restrictByTime: {
          openingTime: '10/29/2018 11:07 PM',
          closingTime: '12/29/2018 11:07 PM'
        }
      }
    };
  });


  it.skip('/', async () => {
    const headers = {
      Accept: 'application/json'
    };

    await request(app)
      .post('/')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(201)
      .then(response => {
        expect(response.body).toEqual(expect.any(Object));
      });
  });
});
