/**
 * Checks if the given string is an address
 *
 * @method isAddress
 * @param {String} address the given HEX adress
 * @return {Boolean}
 */
var isAddress = function (address) {
    console.log(address);

    if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
        // check if it has the basic requirements of an address
        return false;
    } else if (/^(0x)?[a-fA-F0-9]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
        // If it's all small caps or all all caps, return true
        return true;
    }

    return false;
};


window.Parsley.addValidator('ethereum', {
    validateString: function(value) {
        return isAddress(value);
    },
    messages: {
        ru: 'Укажите правильный ethereum адрес',
        en: 'Укажите правильный ethereum адрес',
    }
});

window.Parsley.addValidator("requiredIf", {
    validateString : function(value, requirement) {
        if (jQuery(requirement).prop('checked')){
            return !!value;
        }

        return true;
    },
    priority: 33
})

$(document).ready(function () {
    var $discountBlock = $('#discount-block');
    var $supplyBlock = $('#supply-block');
    var $supplyInput = $supplyBlock.find('input:first');

    $('#add-discount').on('click', function () {
        var $row = $discountBlock.find('.discount-inputs:first').clone();

        $row.find('input').val('');

        $discountBlock.append($row);
    });


    $('#token-type').on('change', function () {
        console.log($supplyInput.attr('name'));

        if ($(this).val() == 'mintable') {
            $supplyBlock.hide();

            $supplyInput.prop('required', false);
        } else {
            $supplyBlock.show();

            $supplyInput.prop('required', true);
        }
    });

    $('[data-parsley]').parsley({
        errorClass: 'has-error',
        errorsWrapper: '<ul class="parsley-error-list"></ul>',
        errorTemplate: '<li class="parsley-error"></li>'
    });
});