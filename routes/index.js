const express = require('express');
const GeneratorService = require('../service/generator');
const router = express.Router();


router.get('/', async (req, res) => {
    res.render('index');
});

router.post('/', async (req, res) => {
  const obj = {
    token: {
      type: 'mintable',
      // supply: '',
      name: 'Bitcoin',
      symbol: 'BTC',
      decimals: 18
    },
    rate: 200,
    wallet: '0x1fe157F5592a7696c9708c145Dc75B3598577901',
    parents: {
      discountByTokens: [{
        discount: 10,
        tokens: 1000
      }, {
        discount: 20,
        tokens: 2000
      }, {
        discount: 30,
        tokens: 3000
      }],
      // discountByTime: [{
        // discount: 20,
        // time: '10/29/2018 11:07 PM'
      // }],
      restrictByCap: {
        softCap: 10000,
        hardCap: 20000
      },
      restrictByTime: {
        openingTime: '10/29/2018 11:07 PM',
        closingTime: '12/29/2018 11:07 PM'
      }
    }
  };

  const data = GeneratorService.generate(obj)
  const response = await GeneratorService.upload('../truffle-template', '../tmp', data);

  res.status(201).json({
    response
  });
});

module.exports = router;
