const AdmZip = require('adm-zip');
const path = require('path');
const fse = require('fs-extra');
const uuidv4 = require('uuid/v4');

module.exports = {
  crowdsaleParentsData() {
    return {
      discountByTokens: {
        params: {},
        className: 'BaseDiscountCrowdsale'
      },
      discountByTime: {
        params: {},
        className: 'BaseTimeDiscountCrowdsale'
      },
      restrictByCap: {
        params: {
          softCap: 'uint256',
          hardCap: 'uint256'
        },
        className: 'BaseCappedCrowdsale'
      },
      restrictByTime: {
        params: {
          openingTime: 'uint256',
          closingTime: 'uint256',
        },
        className: 'BaseTimedCrowdsale'

      }
    };
  },


  /**
   * Генерация crowdsale шаблона
   *
   * @param token
   * @param parents
   * @returns {string}
   */
  generateCrowdsaleTemplate({ token, parents }) {
    // Данные по всем подключаемым контрактам
    const parentsData = this.crowdsaleParentsData();

    let initImports = ['import \'./BaseWithdrawCrowdsale.sol\';'];
    let initExtends = ['BaseWithdrawCrowdsale'];
    let initConstructorParams = ['uint256 rate', 'address wallet', 'ERC20 token'];
    let initConstructorParents = ['BaseWithdrawCrowdsale(rate, wallet, token)'];
    let initFunctionsBlock = [];

    if (token.type === 'mintable') {
      initImports.push('import \'./BaseMintableCrowdsale.sol\';');
      initExtends.push('BaseMintableCrowdsale');
      initConstructorParents.push('BaseMintableCrowdsale()');
    } else {
      initImports.push('import \'./BaseBurnableCrowdsale.sol\';');
      initExtends.push('BaseBurnableCrowdsale');
      initConstructorParents.push('BaseBurnableCrowdsale()');
    }


    // Проходимся по ключам parents
    for (const parent in parents) {
      // Проверяем, есть в parentsData данные, которые хочет передать пользователь
      // Так же, проверяем чтобы params в parentsData небыл пустым
      if (!parentsData.hasOwnProperty(parent)) {
        continue;
      }

      // Получаем стандартные данные по этому контракту
      // Например BaseTimeDiscountCrowdsale
      const { className } = parentsData[parent];

      // Импортируем контракт
      initImports.push(`import './${className}.sol';`);

      // Наследуем контракт
      initExtends.push(className);

      // Получаем все ключи конкретного родителя, например [ 'softCap', 'hardCap' ]
      let keys = Object.keys(parents[parent]);

      // Проверяем чтобы params в parentsData небыл пустым
      if (Object.keys(parentsData[parent].params).length === 0) {
        // Передаем параметры в конструктор родителя (без параметров)
        initConstructorParents.push(`${className}()`);
      } else {
        // Передаем параметры в конструктор родителя
        initConstructorParents.push(`${className}(${keys.join(', ')})`);
      }

      // Проходимся по только что полученным ключам и добавляем их значения в initValues
      keys.forEach(key => {
        if (!initConstructorParams[key]) {
          initConstructorParams.push(`${parentsData[parent].params[key]} ${key}`);
        }
      });

    }

    if (parents.restrictByTime) {
      parents.restrictByTime.openingTime = Date.parse(parents.restrictByTime.openingTime) / 1000;
      parents.restrictByTime.closingTime = Date.parse(parents.restrictByTime.closingTime) / 1000;
    }

    // Добавляем динамическую функцию для задачи сетки скидок
    if (parents.discountByTokens) {
      let discountInitBlock = [];
      let discountLineInitBlock = [];

      for(item of parents.discountByTokens) {
        discountInitBlock.push(item.discount);
        discountLineInitBlock.push(`discountByTokens[${item.discount}] = ${item.tokens} * 1 ether;`)
      }

      initFunctionsBlock.push(`
        function setDiscounts() internal {
          discountAvailable = [${discountInitBlock.join(', ')}];
          ${discountLineInitBlock.join('\n    ')}
        }`);
    } else if (parents.discountByTime) {
      let discountInitBlock = [];
      let discountLineInitBlock = [];


      for(item of parents.discountByTime) {
        item.time = Date.parse(item.time) / 1000;
        discountInitBlock.push(item.discount);
        discountLineInitBlock.push(`discountByTime[${item.discount}] = ${item.time};`)
      }

      initFunctionsBlock.push(`
        function setDiscounts() internal {
          discountAvailable = [${discountInitBlock.join(', ')}];
          ${discountLineInitBlock.join('\n    ')}
        }`);
    }

    console.log(initExtends)
    console.log(initConstructorParams)
    console.log(initConstructorParents)

    return `pragma solidity ^0.4.21;

${initImports.join('\n')}

contract MyCrowdsale is ${initExtends.join(', ')} {
    function MyCrowdsale(${initConstructorParams.join(', ')}) public
    ${initConstructorParents.join('\n    ')}
    {
    }

    ${initFunctionsBlock.join('\n    ')}
}`;
  },


  /**
   * Генерация doployer шаблона
   *
   * @param rate
   * @param wallet
   * @param parents
   * @returns {string}
   */
  generateDeployerTemplate({ rate, wallet, parents }) {
    let initParams = ['rate', 'wallet', 'token'];
    let initValues = { rate, wallet };

    // Данные по всем подключаемым контрактам
    const parentsData = this.crowdsaleParentsData();

    // Проходимся по parents, чтобы заполнить 2 объекта initParams и initValues
    for (parent in parents) {
      // Проверяем, есть в parentsData данные, которые хочет передать пользователь
      // Так же, проверяем чтобы params в parentsData небыл пустым
      if (!parentsData.hasOwnProperty(parent)
        || Object.keys(parentsData[parent].params).length === 0) {
        continue;
      }


      // Получаем все ключи конкретного родителя, например [ 'softCap', 'hardCap' ]
      let keys = Object.keys(parents[parent]);
      // Добавляем данные ключи в список параметров initParams
      // [ 'softCap', 'hardCap', 'openingTime', 'closingTime' ... ]
      initParams = initParams.concat(keys);

      // Проходимся по только что полученным ключам и добавляем их значения в initValues
      keys.forEach(key => {
        initValues[key] = parents[parent][key];
      });
    }

    let initParamsBlock = [];

    // Проходимся по финальному массиву initParams и добавляем значения в initParamsBlock
    for (const param of initParams) {
      // Мы выводим параметр, только если у него есть значение
      if (param in initValues) {
        initParamsBlock.push(`const ${param} = '${initValues[param]}';`);
      }
    }

    return `const Token = artifacts.require(\'Token.sol\');

const Crowdsale = artifacts.require(\'MyCrowdsale.sol\');

module.exports = async (deployer, network, accounts) => {
    await deployer.deploy(Token);

    const _token = await Token.deployed();

    const token = _token.address;
    ${initParamsBlock.join('\n    ')}

    await deployer.deploy(Crowdsale, ${initParams.join(', ')});

    const crowdsale = await Crowdsale.deployed();

    await token.transferOwnership(crowdsale.address);

    const owner = await crowdsale.owner();

    console.log('**************************************************');
    console.log(' -- ПОЖАЛУЙСТА, СКОПИРУЙТЕ ИНФОРМАЦИЮ НИЖЕ --');
    console.log('Адрес токена: ' + _token.address);
    console.log('Адрес Crowdsale: ' + crowdsale.address);
    console.log('Владелец Crowdsale: ' + owner);
    console.log('**************************************************');
}`;
  },

  /**
   * Генерация token шаблона
   *
   * @param token
   * @returns {*|string}
   */
  generateTokenTemplate({ token }) {
    return token.type === 'mintable' ?
      this.generateTokenMintable(token) :
      this.generateTokenBurnable(token);
  },

  /**
   * Генерация burnable token
   *
   * @param name
   * @param symbol
   * @param supply
   * @param decimals
   * @returns {string}
   */
  generateTokenBurnable({ name, symbol, supply, decimals }) {
    return `pragma solidity ^0.4.21;
  
import './BaseBurnableToken.sol';

contract Token is BaseBurnableToken {
    string public name = "${name}";
    string public symbol = "${symbol}";
    uint8 public decimals = ${decimals};
    uint256 public supply = ${supply} * 1 ether;
}`;
  },

  /**
   * Генерация mintable token
   *
   * @param name
   * @param symbol
   * @param decimals
   * @returns {string}
   */
  generateTokenMintable({ name, symbol, decimals }) {
    return `pragma solidity ^0.4.21;
  
import './BaseMintableToken.sol';

contract Token is BaseMintableToken {
    string public name = "${name}";
    string public symbol = "${symbol}";
    uint8 public decimals = ${decimals};
}`;
  },

  /**
   * Вызов методов для генерации
   *
   * @param config
   * @returns {{"/contracts/Token.sol": (*|string), "/contracts/MyCrowdsale.sol": (*|string), "/migrations/2_deploy_contracts.js": (*|string)}}
   */
  generate(config) {
    return {
      '/contracts/Token.sol': this.generateTokenTemplate(config),
      '/contracts/MyCrowdsale.sol': this.generateCrowdsaleTemplate(config),
      '/migrations/2_deploy_contracts.js': this.generateDeployerTemplate(config)
    };
  },

  /**
   * Загрузка данных контракта в директорию
   *
   * @param template
   * @param uploadDir
   * @param mapping
   * @returns {Promise<boolean>}
   */
  async upload(template = '../truffle-template', uploadDir = '../tmp', mapping) {
    let uploadPath = path.join(path.resolve(__dirname, uploadDir), '/', uuidv4());
    const basepath = path.resolve(__dirname, template);

    await fse.copy(basepath, uploadPath);

    for (let item in mapping) {
      await fse.outputFile(uploadPath + item, Buffer.from(mapping[item]));
    }

    return true;
  },

  /**
   * Архивирование данных контракта
   *
   * @param template
   * @param mapping
   * @returns {*|Buffer}
   */
  zip(template = '../truffle-template', mapping) {
    const zip = new AdmZip();

    const basepath = path.resolve(__dirname, template)

    zip.addLocalFolder(basepath);

    for (let item in mapping) {
      zip.addFile(item, Buffer.from(mapping[item]));
    }

    return zip.toBuffer();
  }
};
